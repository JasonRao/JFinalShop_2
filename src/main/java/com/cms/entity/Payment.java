package com.cms.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cms.entity.base.BasePayment;
import com.cms.util.DBUtils;
import com.jfinal.plugin.activerecord.Page;

/**
 * Entity - 支付
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class Payment extends BasePayment<Payment> {
	
	/**
	 * 类型
	 */
	public enum Type {
		ORDER("订单"),
		RECHARGE("充值"),
		WITHDRAW("提现"),
		REFUND("退款"),
		COMMISSION("分销佣金"),
		SYSTEM("平台操作");
		public String text;
		Type(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
		public static Map<Integer, Payment.Type> typeValueMap = new HashMap<>();
	    static {
			Payment.Type[] values = Payment.Type.values();
	        for (Payment.Type type : values) {
	        	typeValueMap.put(type.ordinal(), type);
	        }
	    }
	}
	
	/**
	 * 状态
	 */
	public enum Status {
		PENDING_PAYMENT("待付款"),
		COMPLETED("已完成"),
		CANCELED("已取消"); 
		public String text;
		Status(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
	}
	
	
    /**
     * 支付方式
     */
	public enum Method {
		ALIPAY("支付宝"),
		ALIPAY_WEB("支付宝PC"),
		ALIPAY_APP("支付宝APP"),
		ALIPAY_WAP("支付宝WAP"),
		WEIXINPAY("微信支付"),
		WEIXINPAY_WECHAT("微信支付公众号"),
		WEIXINPAY_NATIVE("微信支付PC"),
		WEIXINPAY_APP("微信支付APP"),
		WEIXINPAY_H5("微信支付H5"),
		BALANCE("余额支付");
		public String text;
		Method(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
		public static Map<Integer, Payment.Method> methodValueMap = new HashMap<>();
	    static {
			Payment.Method[] values = Payment.Method.values();
	        for (Payment.Method method : values) {
	        	methodValueMap.put(method.ordinal(), method);
	        }
	    }
	}

	public String getTypeName(){
		return Payment.Type.typeValueMap.get(getType()).getText();
	}
 
    
    /**
     * 会员
     */
    private Member member;
    
    /**
     * 获取会员
     * 
     * @return  会员
     */
    public Member getMember(){
        if(member == null){
            member = new Member().dao().findById(getMemberId());
        }
        return member;
    }
    
    /**
     * 
     * @param sn
     *          
     */
    public Payment findBySn(String sn){
        return findFirst("select * from cms_payment where sn=? ",sn);
    }
    
    public List<Payment> findByMemberId(Long memberId){
        return find("select * from cms_payment where memberId=? order by createDate desc", memberId);
    }
    
    public List<Payment> findByOrderId(Long orderId){
        return find("select * from cms_payment where orderId=? ", orderId);
    }
    
	public Page<Payment> findPage(Long memberId,Integer pageNumber,Integer pageSize){
	    String filterSql = "";
	    if(memberId!=null){
	        filterSql+=" and memberId = "+memberId;
	    }
		String orderBySql = DBUtils.getOrderBySql("createDate desc");
		return paginate(pageNumber, pageSize, "select *", "from cms_payment where 1=1 "+filterSql+orderBySql);
	}
	
	public Page<Payment> findPage(String nickname,String mobile,Payment.Type type,Integer pageNumber,Integer pageSize){
	    String filterSql = "";
	    if(StringUtils.isNotBlank(nickname)){
	        filterSql+=" and memberId in (select id from cms_member where nickname like '%"+nickname+"%') ";
	    }
	    if(StringUtils.isNotBlank(mobile)){
	        filterSql+=" and memberId in (select id from cms_member where mobile like '%"+mobile+"%') ";
	    }
	    if(type!=null){
	    	filterSql+=" and type = "+type.ordinal();
	    }
		String orderBySql = DBUtils.getOrderBySql("createDate desc");
		return paginate(pageNumber, pageSize, "select *", "from cms_payment where 1=1 "+filterSql+orderBySql);
	}
}
