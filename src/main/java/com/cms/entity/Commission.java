package com.cms.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateFormatUtils;

import com.cms.entity.base.BaseCommission;
import com.cms.util.DBUtils;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * 分销佣金
 */
@SuppressWarnings("serial")
public class Commission extends BaseCommission<Commission> {
	
	/**
     * 佣金状态
     */
	public enum Status{
		PENDING_SETTLEMENT("待结算"),
		PENDING_CHECK("待审核"),
		COMPLETED("已完成"),
		REJECTED("已驳回");
		public String text;
		Status(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
		public static Map<Integer, Commission.Status> statusValueMap = new HashMap<>();
	    static {
            Commission.Status[] values = Commission.Status.values();
	        for (Commission.Status status : values) {
	        	statusValueMap.put(status.ordinal(), status);
	        }
	    }
	}
    
	
	public String getStatusName(){
		return Status.statusValueMap.get(getStatus()).getText();
	}
	
    /**
     * 会员
     */
    private Member member;
    
    /**
     * 订单
     */
    private Order order;
    
    
    public BigDecimal findTodayDistributeAmount(Long memberId){
    	String tody = DateFormatUtils.format(new Date(), "yyyy-MM-dd");
    	String startTime = tody+" 00:00:00";
    	String endTime = tody+" 23:59:59";
        return Db.queryBigDecimal("select sum(distributeAmount) from cms_commission where memberId=? and createDate>='"+startTime+"' and createDate <='"+endTime+"'",memberId);
    }
    
    public BigDecimal findMonthDistributeAmount(Long memberId){
    	String tody = DateFormatUtils.format(new Date(), "yyyy-MM");
    	String startTime = tody+"-01 00:00:00";
    	String endTime = tody+"-31 23:59:59";
        return Db.queryBigDecimal("select sum(distributeAmount) from cms_commission where memberId=? and createDate>='"+startTime+"' and createDate <='"+endTime+"'",memberId);
    }
    
    public BigDecimal findTotalDistributeAmount(Long memberId){
        return Db.queryBigDecimal("select sum(distributeAmount) from cms_commission where memberId=?",memberId);
    }
    
    
    
    
    /**
     * 获取会员
     * 
     * @return  会员
     */
    public Member getMember(){
        if(member == null){
            member = new Member().dao().findById(getMemberId());
        }
        return member;
    }
    
    /**
     * 获取订单
     * 
     * @return  订单
     */
    public Order getOrder(){
        if(order == null){
        	order = new Order().dao().findById(getOrderId());
        }
        return order;
    }
    
    /**
     * 查找分销分页
     * 
     * @param pageNumber
     *            页码
     * @param pageSize
     *            每页记录数
     * @return 分销分页
     */
    public Page<Commission> findPage(Integer pageNumber,Integer pageSize){
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select *", "from cms_commission where 1=1 "+orderBySql);
    }
    
    /**
     * 查找分销分页
     * 
     * @param pageNumber
     *            页码
     * @param pageSize
     *            每页记录数
     * @param memberId
     *            会员ID
     * @return 分销分页
     */
    public Page<Commission> findPage(Integer pageNumber,Integer pageSize,Long memberId,Integer status){
        String filterSql = "";
        if(memberId!=null){
            filterSql+=" and memberId="+memberId;
        }
        if(status!=null){
            filterSql+=" and status="+status;
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select *", "from cms_commission where 1=1 "+filterSql+orderBySql);
    }
    
    /**
     * 查找分销金额
     * 
     * @return 分销金额
     */
    public BigDecimal findDistributeAmount(Long memberId,Integer status){
        return Db.queryBigDecimal("select sum(distributeAmount) from cms_commission where memberId=? and status=?",memberId,status);
    }
    
    /**
     * 根据订单ID查询分销佣金
     * 
     * @param orderId
     *          订单ID
     * @return  分销佣金
     */
    public List<Commission> findByOrderId(Long orderId){
        return find("select * from cms_commission where orderId=? ", orderId);
    }
    
    /**
     * 查找分销佣金
     * 
     * @return 分销佣金
     */
    public List<Commission> findList(Long memberId,Integer status){
        String filterSql = "";
        if(memberId!=null){
            filterSql+=" and memberId="+memberId;
        }
        if(status!=null){
            filterSql+=" and status="+status;
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return find("select * from cms_commission where 1=1 "+filterSql+orderBySql);
    }
    
    public Page<Commission> findShouyiPage(Integer pageNumber,Integer pageSize,Long memberId){
        String filterSql = "";
        if(memberId!=null){
            filterSql+=" and memberId="+memberId;
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select * "," from cms_commission where 1=1 "+filterSql+orderBySql);
    }
}
