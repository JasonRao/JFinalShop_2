package com.cms.util;

import com.cms.entity.GroupProduct;
import com.cms.entity.Order;
import com.cms.entity.OrderItem;

import java.util.List;

public class GroupUtils {

    public static void updateCurrentNum(Order order){
        List<OrderItem> orderItems = order.getOrderItems();
        OrderItem orderItem = orderItems.get(0);
        GroupProduct groupProduct = orderItem.getProduct().getGroupProduct();
        Integer currentNum = groupProduct.getCurrentNum();
        if(currentNum==null){
            currentNum = 0;
        }
        currentNum = currentNum + 1;
        groupProduct.setCurrentNum(currentNum);
        //拼团完成
        if(groupProduct.getNum().intValue() == currentNum.intValue()){
            groupProduct.setStatus(GroupProduct.Status.COMPLETED.ordinal());
        }
        groupProduct.update();
    }

}
