package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.ProductCategory;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 顶级商品分类列表
 * 
 * 
 * 
 */
@TemplateVariable(name="product_category_root_list")
public class ProductCategoryRootListDirective extends BaseDirective {

	/** 变量名称 */
	private static final String VARIABLE_NAME = "productCategorys";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Integer count = getCount(scope);
        List<ProductCategory> productCategorys = new ProductCategory().dao().findRoots(count);
        scope.setLocal(VARIABLE_NAME,productCategorys);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }

}