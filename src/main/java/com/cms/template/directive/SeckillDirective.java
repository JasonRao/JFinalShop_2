package com.cms.template.directive;

import com.cms.TemplateVariable;
import com.cms.entity.Seckill;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 秒杀
 * 
 * 
 * 
 */
@TemplateVariable(name="seckill")
public class SeckillDirective extends BaseDirective {

	/** "ID"参数名称 */
	private static final String ID_PARAMETER_NAME = "id";
	
	/** 变量名称 */
	private static final String VARIABLE_NAME = "seckill";
	
    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Long id = getParameter(ID_PARAMETER_NAME, Long.class, scope);
        Seckill seckill = new Seckill().dao().findById(id);
        scope.setLocal(VARIABLE_NAME,seckill);
        stat.exec(env, scope, writer);
    }
    
    public boolean hasEnd() {
        return true;
    }
}