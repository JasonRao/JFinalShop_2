package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.ArticleCategory;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 顶级文章分类列表
 * 
 * 
 * 
 */
@TemplateVariable(name="article_category_root_list")
public class ArticleCategoryRootListDirective extends BaseDirective {

	/** 变量名称 */
	private static final String VARIABLE_NAME = "articleCategorys";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Integer count = getCount(scope);
        List<ArticleCategory> articleCategorys = new ArticleCategory().dao().findRoots(count);
        scope.setLocal(VARIABLE_NAME,articleCategorys);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}