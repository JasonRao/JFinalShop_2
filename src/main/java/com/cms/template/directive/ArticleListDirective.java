package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.Article;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 文章列表
 * 
 * 
 * 
 */
@TemplateVariable(name="article_list")
public class ArticleListDirective extends BaseDirective {

	/** "文章分类ID"参数名称 */
	private static final String ARTICLECATEGORYID_PARAMETER_NAME = "articleCategoryId";

	/** 变量名称 */
	private static final String VARIABLE_NAME = "articles";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Long articleCategoryId = getParameter(ARTICLECATEGORYID_PARAMETER_NAME, Long.class, scope);
        Integer start = getStart(scope);
        Integer count = getCount(scope);
        String orderBy = getOrderBy(scope);
        List<Article> articles = new Article().dao().findList(articleCategoryId, start, count, orderBy);
        scope.setLocal(VARIABLE_NAME,articles);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}