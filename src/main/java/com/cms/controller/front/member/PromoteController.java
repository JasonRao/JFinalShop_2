package com.cms.controller.front.member;

import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/promote")
public class PromoteController extends BaseController{

	public void index(){
		setAttr("member", new Member().dao().findById(getCurrentMember().getId()));
	    render("/templates/"+getTheme()+"/"+getDevice()+"/member_promote.html");
	}
}
