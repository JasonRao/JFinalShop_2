package com.cms.controller.front.member;

import java.util.ArrayList;
import java.util.List;

import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.entity.Order;
import com.cms.entity.Pack;
import com.cms.entity.Payment;
import com.cms.routes.RouteMapping;
import com.cms.util.DeviceUtils;
import com.cms.util.fegine.FegineApi;
import com.cms.util.fegine.FegineResponse;


/**
 * Controller - 订单
 * 
 * 
 * 
 */
@RouteMapping(url = "/member/order")

public class OrderController extends BaseController{
	
	public void index(){
		Order.Status status = getParaToEnum(Order.Status.class,"status");
		if(status!=null){
			setAttr("status", status.name());
		}
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_order.html");
	}

	/**
	 * 列表
	 */
	public void list(){
		String keyword = getPara("keyword");
		Integer pageNumber = getParaToInt("pageNumber");
		Order.Status status = getParaToEnum(Order.Status.class,"status");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 10 ; 
		Member currentMember = getCurrentMember();
		setAttr("page",new Order().dao().findPage(null,null,status,currentMember.getId(),keyword,pageNumber,pageSize));
		if(status!=null){
			setAttr("status", status.name());
		}
		setAttr("keyword", keyword);
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_order_list.html");
	}
	
    public void detail(){
        Long id = getParaToLong(0);
        Order order = new Order().dao().findById(id);
        setAttr("order", order);
        List<Pack> packs = new Pack().dao().findByOrderId(id);
		setAttr("packs", packs);
        render("/templates/"+getTheme()+"/"+getDevice()+"/member_order_detail.html");
    }
    
	public void photo(){
		Long orderId = getParaToLong("orderId");
		Order order = new Order().dao().findById(orderId);
		setAttr("order", order);
		Long packId = getParaToLong("packId");
		Pack pack = new Pack().dao().findById(packId); 
		List<String> photos = new ArrayList<String>();
		photos.add(pack.getPhoto());
		setAttr("photos", photos);
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_order_photo.html");
	}
    
    
	public void traffic(){
		Long orderId = getParaToLong("orderId");
		Order order = new Order().dao().findById(orderId);
		setAttr("order", order);
		Long packId = getParaToLong("packId");
		Pack pack = new Pack().dao().findById(packId); 
		FegineResponse response = FegineApi.getInfo(pack.getExpressNumber(), Pack.ExpressCode.expressCodeValueMap.get(pack.getExpressCode()).name());
		setAttr("infos", response.getResult().getList());
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_order_traffic.html");
	}
}
