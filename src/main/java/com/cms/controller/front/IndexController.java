package com.cms.controller.front;

import com.cms.routes.RouteMapping;
import com.cms.util.OSSUtils;

/**
 * Controller - 首页
 * 
 * 
 * 
 */
@RouteMapping(url = "/")
public class IndexController extends BaseController{

	/**
	 * 首页
	 */
	public void index(){
	    render("/templates/"+getTheme()+"/"+getDevice()+"/index.html");
	}

	public void test(){
		try {
			OSSUtils.test();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}
