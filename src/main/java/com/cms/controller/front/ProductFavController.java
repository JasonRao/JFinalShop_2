package com.cms.controller.front;

import java.util.Date;
import java.util.HashMap;

import com.cms.Feedback;
import com.cms.entity.ProductFav;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/product_fav")
public class ProductFavController extends BaseController{
	
	public void save(){
		if(getCurrentMember()==null){
			renderJson(Feedback.warn("您还没有登录!"));
			return;
		}
		ProductFav productFav = getModel(ProductFav.class,"",true); 
		productFav.setMemberId(getCurrentMember().getId());
		ProductFav oldProductFav = new ProductFav().dao().findByMemberId(productFav.getMemberId(), productFav.getProductId());
		if(oldProductFav==null){
			productFav.setCreateDate(new Date());
			productFav.save();
		}else{
			oldProductFav.delete();
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}
