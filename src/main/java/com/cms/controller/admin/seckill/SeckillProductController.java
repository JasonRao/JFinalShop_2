package com.cms.controller.admin.seckill;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cms.Feedback;
import com.cms.controller.admin.BaseController;
import com.cms.entity.Ad;
import com.cms.entity.SeckillProduct;
import com.cms.routes.RouteMapping;


/**
 * Controller - 秒杀商品
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/seckill/seckill_product")

public class SeckillProductController extends BaseController {


	/**
	 * 添加
	 */
	public void add() {
		Long seckillId = getParaToLong("seckillId");
		setAttr("seckillId", seckillId);
		render(getView("seckill/seckill_product/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		SeckillProduct seckillProduct = getModel(SeckillProduct.class,"",true); 
		seckillProduct.setCreateDate(new Date());
		seckillProduct.setModifyDate(new Date());
		seckillProduct.save();
		redirect(getListQuery("/admin/seckill/seckill_product/list?seckillId="+seckillProduct.getSeckillId()));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("seckillProduct", new SeckillProduct().dao().findById(id));
		render(getView("seckill/seckill_product/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		SeckillProduct seckillProduct = getModel(SeckillProduct.class,"",true); 
		seckillProduct.setModifyDate(new Date());
		seckillProduct.update();
		redirect(getListQuery("/admin/seckill/seckill_product/list?seckillId="+seckillProduct.getSeckillId()));
	}
	
	/**
     * 修改排序
     */
    public void updateSort(){
        String sortArray = getPara("sortArray");
        JSONArray jsonArray = JSONArray.parseArray(sortArray);
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long id = jsonObject.getLong("id");
            Integer sort = jsonObject.getInteger("sort");
            Ad ad = new Ad().dao().findById(id);
            ad.setSort(sort);
            ad.update();
        }
        renderJson(Feedback.success(new HashMap<>()));
    }

	/**
	 * 列表
	 */
	public void list() {
	    Long seckillId = getParaToLong("seckillId");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new SeckillProduct().dao().findPage(seckillId,pageNumber,PAGE_SIZE));
		setAttr("seckillId", seckillId);
		render(getView("seckill/seckill_product/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new SeckillProduct().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}