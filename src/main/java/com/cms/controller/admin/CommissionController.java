package com.cms.controller.admin;

import java.util.HashMap;

import com.cms.Feedback;
import com.cms.entity.Commission;
import com.cms.routes.RouteMapping;
import com.cms.util.CommissionUtils;

/**
 * Controller - 分销
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/commission")
public class CommissionController extends BaseController {

    /**
     * 列表
     */
    public void list(){
        Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
            pageNumber = 1;
        }
        setAttr("page", new Commission().dao().findPage(pageNumber,PAGE_SIZE));
        render(getView("commission/list"));
    }
    
    /**
     * 审核
     */
    public void check(){
    	Long id = getParaToLong("id");
    	Commission.Status status = getParaToEnum(Commission.Status.class,"status");
    	Commission commission = new Commission().dao().findById(id);
    	commission.setStatus(status.ordinal());
    	commission.update();
    	if(status.ordinal()==Commission.Status.COMPLETED.ordinal()){
    		CommissionUtils.commission(commission.getOrder());
    	}
    	renderJson(Feedback.success(new HashMap<>()));
    }
}
