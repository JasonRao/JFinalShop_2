package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;

import com.cms.Feedback;
import com.cms.entity.DistributeRank;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/distribute_rank")
public class DistributeRankController extends BaseController {

	/**
	 * 添加
	 */
	public void add() {
		render(getView("distribute_rank/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		DistributeRank distributeRank = getModel(DistributeRank.class,"",true); 
		distributeRank.setCreateDate(new Date());
		distributeRank.setModifyDate(new Date());
		distributeRank.save();
		redirect(getListQuery("/admin/distribute_rank/list"));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("distributeRank", new DistributeRank().dao().findById(id));
		render(getView("distribute_rank/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		DistributeRank distributeRank = getModel(DistributeRank.class,"",true); 
		distributeRank.setModifyDate(new Date());
		distributeRank.update();
		redirect(getListQuery("/admin/distribute_rank/list"));
	}
	
	/**
	 * 列表
	 */
	public void list() {
	    String name = getPara("name");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new DistributeRank().dao().findPage(name,pageNumber,PAGE_SIZE));
		setAttr("name", name);
		render(getView("distribute_rank/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new DistributeRank().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
}
