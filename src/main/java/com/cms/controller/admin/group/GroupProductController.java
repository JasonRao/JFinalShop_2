package com.cms.controller.admin.group;

import java.util.Date;
import java.util.HashMap;

import com.cms.entity.Group;
import org.apache.commons.lang.ArrayUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cms.Feedback;
import com.cms.controller.admin.BaseController;
import com.cms.entity.Ad;
import com.cms.entity.GroupProduct;
import com.cms.routes.RouteMapping;


/**
 * Controller - 拼团商品
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/group/group_product")

public class GroupProductController extends BaseController {


	/**
	 * 添加
	 */
	public void add() {
		Long groupId = getParaToLong("groupId");
		setAttr("groupId", groupId);
		render(getView("group/group_product/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		GroupProduct groupProduct = getModel(GroupProduct.class,"",true);
		groupProduct.setStatus(GroupProduct.Status.PENDING.ordinal());
		groupProduct.setCreateDate(new Date());
		groupProduct.setModifyDate(new Date());
		groupProduct.save();
		redirect(getListQuery("/admin/group/group_product/list?groupId="+groupProduct.getGroupId()));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("groupProduct", new GroupProduct().dao().findById(id));
		render(getView("group/group_product/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		GroupProduct groupProduct = getModel(GroupProduct.class,"",true); 
		groupProduct.setModifyDate(new Date());
		groupProduct.update();
		redirect(getListQuery("/admin/group/group_product/list?groupId="+groupProduct.getGroupId()));
	}
	
	/**
     * 修改排序
     */
    public void updateSort(){
        String sortArray = getPara("sortArray");
        JSONArray jsonArray = JSONArray.parseArray(sortArray);
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long id = jsonObject.getLong("id");
            Integer sort = jsonObject.getInteger("sort");
            Ad ad = new Ad().dao().findById(id);
            ad.setSort(sort);
            ad.update();
        }
        renderJson(Feedback.success(new HashMap<>()));
    }

	/**
	 * 列表
	 */
	public void list() {
	    Long groupId = getParaToLong("groupId");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new GroupProduct().dao().findPage(groupId,pageNumber,PAGE_SIZE));
		setAttr("groupId", groupId);
		render(getView("group/group_product/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new GroupProduct().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}