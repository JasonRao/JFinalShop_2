/*
 * 
 * 
 * 
 */
package com.cms.controller.admin;


import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;

import com.cms.entity.Order;
import com.cms.routes.RouteMapping;
import com.cms.util.ShopUtils;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * Controller - 共用
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/common")

public class CommonController extends BaseController {



	/**
	 * 主页
	 */
	public void main() {
	    render(getView("common/main"));
	}
	
	/**
	 * 首页
	 */
	public void index() {
		String dayStart = DateFormatUtils.format(new Date(), "yyyy-MM-dd")+" 00:00:00";
		String dayEnd = DateFormatUtils.format(new Date(), "yyyy-MM-dd")+" 23:59:59";
		setAttr("daySaleAmount", Db.queryBigDecimal("select sum(amount) from cms_order where status>0 and createDate>='"+dayStart+"' and createDate<='"+dayEnd+"' "));
		String weekStart = DateFormatUtils.format(ShopUtils.getBeginDayOfWeek(), "yyyy-MM-dd")+" 00:00:00";
		String weekEnd = DateFormatUtils.format(ShopUtils.getEndDayOfWeek(), "yyyy-MM-dd")+" 23:59:59";
		setAttr("weekSaleAmount", Db.queryBigDecimal("select sum(amount) from cms_order where status>0 and createDate>='"+weekStart+"' and createDate<='"+weekEnd+"' "));
		String monthStart = DateFormatUtils.format(ShopUtils.getBeginDayOfMonth(), "yyyy-MM-dd")+" 00:00:00";
		String monthEnd = DateFormatUtils.format(ShopUtils.getEndDayOfMonth(), "yyyy-MM-dd")+" 23:59:59";
		setAttr("monthSaleAmount", Db.queryBigDecimal("select sum(amount) from cms_order where status>0 and createDate>='"+monthStart+"' and createDate<='"+monthEnd+"' "));
		setAttr("dayPendingShipmentOrderCount", Db.queryInt("select count(*) from cms_order where status="+Order.Status.PENDING_SHIPMENT.ordinal()+" and createDate>='"+dayStart+"' and createDate<='"+dayEnd+"' "));
		String orderItemSql = "";
		String productItemSql = "";
		for(int i=0;i<=6;i++){
			Date date = DateUtils.addDays(new Date(), -i);
			if(i>0){
				orderItemSql+=",";
				productItemSql+=",";
			}
			orderItemSql += "(select count(*) from cms_order where createDate <= '"+DateFormatUtils.format(date, "yyyy-MM-dd")+" 23:59:59') as '"+DateFormatUtils.format(date, "MM-dd")+"'";
			productItemSql += "(select count(*) from cms_product where createDate <= '"+DateFormatUtils.format(date, "yyyy-MM-dd")+" 23:59:59') as '"+DateFormatUtils.format(date, "MM-dd")+"'";
		}
		String orderSql = "select "+orderItemSql;
		Record orderRecord = Db.findFirst(orderSql);
		String [] orderColumnNames = orderRecord.getColumnNames();
		Object [] orderColumnValues = orderRecord.getColumnValues();
		setAttr("orderTime", "'"+StringUtils.join(orderColumnNames,"','")+"'");
		setAttr("orderValue", StringUtils.join(orderColumnValues,","));
		
		String productSql = "select "+productItemSql;
		Record productRecord = Db.findFirst(productSql);
		String [] productColumnNames = productRecord.getColumnNames();
		Object [] productColumnValues = productRecord.getColumnValues();
		setAttr("productTime", "'"+StringUtils.join(productColumnNames,"','")+"'");
		setAttr("productValue", StringUtils.join(productColumnValues,","));
				
		render(getView("common/index"));
	}
}