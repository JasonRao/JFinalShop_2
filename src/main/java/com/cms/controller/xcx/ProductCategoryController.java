package com.cms.controller.xcx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.ProductCategory;
import com.cms.routes.RouteMapping;

/**
 * @description : 
 * @author : heyewei
 * @create : 2020年12月22日
 **/
@RouteMapping(url = "/xcx/product_category")
public class ProductCategoryController extends BaseController{

	public void index(){
		List<ProductCategory> rootProductCategorys = new ProductCategory().dao().findRoots();
		List<Map<String,Object>> data = new ArrayList<>();
		for(ProductCategory rootProductCategory : rootProductCategorys){
			Map<String,Object> map = new HashMap<>();
			map.put("id", rootProductCategory.getId());
			map.put("name", rootProductCategory.getName());
			data.add(map);
		}
		renderJson(Feedback.success(data));
	}
	
	public void detail(){
		Long productCategoryId = getParaToLong("productCategoryId");
		ProductCategory productCategory = new ProductCategory().dao().findById(productCategoryId);
		Map<String,Object> data = new HashMap<>();
		data.put("id", productCategory.getId());
		data.put("name", productCategory.getName());
		data.put("products", productCategory.getProducts());
		renderJson(Feedback.success(data));
	}
}
