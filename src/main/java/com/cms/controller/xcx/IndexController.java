package com.cms.controller.xcx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Ad;
import com.cms.entity.Article;
import com.cms.entity.Brand;
import com.cms.entity.Product;
import com.cms.entity.ProductCategory;
import com.cms.routes.RouteMapping;

/**
 * @description : 
 * @author : heyewei
 * @create : 2020年8月13日
 **/
@RouteMapping(url = "/xcx/index")
public class IndexController extends BaseController{

	
	public void banner(){
		List<Ad> ads = new Ad().find("select * from cms_ad where adPositionId=3");
		Map<String,Object> map = new HashMap<>();
		map.put("banner", ads);
		renderJson(Feedback.success(map));
	}
	
	
	public void channel(){
		List<Article> articles = new Article().dao().find("select * from cms_article where articleCategoryId=11");
		Map<String,Object> map = new HashMap<>();
		map.put("articles", articles);
		renderJson(Feedback.success(map));
	}
	
	
	public void brand(){
		List<Brand> brands = new Brand().dao().find("select * from cms_brand limit 4");
		Map<String,Object> map = new HashMap<>();
		map.put("brand", brands);
		renderJson(Feedback.success(map));
	}
	
	public void newProducts(){
		List<Product> products = new Product().dao().find("select * from cms_product order by createDate desc limit 4");
		Map<String,Object> map = new HashMap<>();
		map.put("newProducts", products);
		renderJson(Feedback.success(map));
	}
	
	public void hotProducts(){
		List<Product> products = new Product().dao().find("select * from cms_product order by hits desc limit 4");
		Map<String,Object> map = new HashMap<>();
		map.put("hotProducts", products);
		renderJson(Feedback.success(map));
	}
	
	public void categoryProducts(){
		List<ProductCategory> productCategorys = new ProductCategory().dao().find("select * from cms_product_category where parentId is null");
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		for(ProductCategory productCategory : productCategorys){
			Map<String,Object> map = new HashMap<>();
			map.put("id", productCategory.getId());
			map.put("name", productCategory.getName());
			List<Product> products = new Product().dao().find("select * from cms_product where productCategoryId="+productCategory.getId()+" order by hits desc limit 4");
			map.put("products", products);
		}
		Map<String,Object> map = new HashMap<>();
		map.put("categoryProducts", list);
		renderJson(Feedback.success(map));
	}
	
}
