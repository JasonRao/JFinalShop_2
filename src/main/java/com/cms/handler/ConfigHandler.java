package com.cms.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cms.util.SystemUtils;
import com.jfinal.handler.Handler;

/**
* 控制器
*/
public class ConfigHandler extends Handler {
	
	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		request.setAttribute("config", SystemUtils.getConfig());
		next.handle(target, request, response, isHandled);
	}
	
}
