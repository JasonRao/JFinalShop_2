package com.cms;

public class PosterImage {

    private Integer width;
    private Integer height;
    private Integer index;
    private Integer left;
    private Integer top;
    private String src;
    
    
    public Integer getWidth() {
        return width;
    }
    public void setWidth(Integer width) {
        this.width = width;
    }
    public Integer getHeight() {
        return height;
    }
    public void setHeight(Integer height) {
        this.height = height;
    }
    public Integer getIndex() {
        return index;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }
    public Integer getLeft() {
        return left;
    }
    public void setLeft(Integer left) {
        this.left = left;
    }
    public Integer getTop() {
        return top;
    }
    public void setTop(Integer top) {
        this.top = top;
    }
    public String getSrc() {
        return src;
    }
    public void setSrc(String src) {
        this.src = src;
    }
}
